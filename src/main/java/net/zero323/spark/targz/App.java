package net.zero323.spark.targz;

import com.google.common.base.Charsets;
import org.apache.spark.api.java.*;
import org.apache.spark.SparkConf;
import org.apache.spark.input.PortableDataStream;
import scala.Tuple2;

public class App {
    public static void main(String[] args) {
        String path = args[0];
        JavaSparkContext sc = new JavaSparkContext("local", "test", new SparkConf());
        JavaPairRDD<String, PortableDataStream> rdd = sc.binaryFiles(path);
        JavaRDD<Tuple2<String, String>> extracted = new TarGzExtractor().extractAndDecode(rdd, Charsets.UTF_8);
        System.out.println(extracted.collect());
        sc.stop();
  }
}
