name := "targz"

organization := "net.zero323"

version := "1.0"

scalaVersion := "2.11.8"

scalacOptions ++= Seq("-feature", "-unchecked", "-deprecation", "-Xfatal-warnings")

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % "1.6.1",
  "org.apache.commons" % "commons-compress" % "1.11"
)

